package http

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func WriteResponseHeader(c *gin.Context, code int8, m string) {
	c.Writer.Header().Set("x-yellow-code", string(code))
	c.Writer.Header().Set("x-yellow-message", m)
}

func ResponseBadRequest(c *gin.Context) {
	//WriteResponseHeader(c, -1, "Bad Request")
	c.JSON(http.StatusBadRequest, &gin.H{
		"code_return": 400,
		"message":     "Bad Request",
	})
}
