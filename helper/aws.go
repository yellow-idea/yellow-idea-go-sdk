package helper

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

func S3Base64ToCDN(s string) string {
	i := strings.Index(s, "https")
	if i > -1 {
		return s
	}
	url := "https://6azzlyezme.execute-api.ap-southeast-1.amazonaws.com/prod/upload"
	method := "POST"
	payload := strings.NewReader(fmt.Sprintf("{\n    \"path\": \"/test\",\n    \"img_url\": \"%s\"\n}", s))
	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	defer func() { res.Body.Close() }()
	body, err := ioutil.ReadAll(res.Body)

	resBody := string(body)

	i = strings.Index(resBody, "https")
	if i > -1 {
		var f map[string]string
		err := json.Unmarshal([]byte(resBody), &f)
		if err != nil {
			fmt.Println(err)
		} else {
			return f["data"]
		}
	} else {
		fmt.Println("Index not found")
	}
	return ""
}

// AddFileToS3 will upload a single file to S3, it will require a pre-built aws session
// and will set file info like content type and encryption on the uploaded file.
func S3FileToCDN(fileDir string, key string) *map[string]interface{} {
	emptyResult := &map[string]interface{}{
		"status": "ok",
		"url":    "",
	}
	// Create a single AWS session (we can re use this if we're uploading many files)
	awsID := string([]byte{65, 75, 73, 65, 82, 53, 68, 55, 80, 82, 55, 68, 87, 54, 54, 90, 81, 77, 54, 84})
	awsSe := string([]byte{114, 56, 122, 108, 71, 57, 76, 83, 116, 87, 114, 105, 82, 105, 48, 113, 69, 88, 90, 53, 104, 89, 69, 69, 69, 70, 52, 106, 80, 107, 83, 103, 98, 86, 54, 105, 107, 84, 66, 122})
	s, err := session.NewSession(&aws.Config{
		Region: aws.String("ap-southeast-1"),
		Credentials: credentials.NewStaticCredentials(
			awsID,
			awsSe,
			"",
		),
	})
	if err != nil {
		fmt.Println(err)
		return emptyResult
	}

	// Open the file for use
	file, err := os.Open(fileDir)
	if err != nil {
		fmt.Println(err)
		return emptyResult
	}
	defer file.Close()

	// Get file size and read the file content into a buffer
	fileInfo, _ := file.Stat()
	var size int64 = fileInfo.Size()
	buffer := make([]byte, size)
	_, err = file.Read(buffer)

	if err != nil {
		fmt.Println(err)
		return emptyResult
	}

	// Config settings: this is where you choose the bucket, filename, content-type etc.
	// of the file you're uploading.
	_, err = s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket: aws.String("cdn.yellow-idea.com"),
		Key:    aws.String(key),
		Body:   bytes.NewReader(buffer),
	})

	if err != nil {
		fmt.Println(err)
		return emptyResult
	}

	_ = os.Remove(fileDir)

	return &map[string]interface{}{
		"status": "ok",
		"url":    fmt.Sprintf(`https://cdn.yellow-idea.com%s`, key),
	}
}
