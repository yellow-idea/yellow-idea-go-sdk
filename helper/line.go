package helper

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func LinePushMessage(channelAccessToken string, mID string, message string) int {
	url := "https://api.line.me/v2/bot/message/push"
	method := "POST"

	s := fmt.Sprintf(`{"to": "%s","messages": %s}`, mID, message)

	payload := strings.NewReader(s)

	client := &http.Client{
	}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		fmt.Println(s)
		return 500
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", channelAccessToken))

	res, err := client.Do(req)
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)

	fmt.Println(res.Status)
	fmt.Println(string(body))

	return res.StatusCode
}
