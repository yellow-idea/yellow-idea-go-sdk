package helper

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
)

func PrettyJson(b []byte) ([]byte, error) {
	var out bytes.Buffer
	err := json.Indent(&out, b, "", "	")
	return out.Bytes(), err
}

func InterfaceToJson(a *interface{}) string {
	b, _ := json.Marshal(a)
	c, _ := PrettyJson(b)
	return string(c)
}

func InterfaceToJsonObject(data *interface{}) map[string]interface{} {
	a := InterfaceToJson(data)
	b := []byte(a)
	var m map[string]interface{}
	_ = json.Unmarshal(b, &m)
	return m
}

func InterfaceToString(d interface{}) string {
	return fmt.Sprintf("%v", d)
}

func StringToMockJson(s string) interface{} {
	var result map[string]interface{}
	_ = json.Unmarshal([]byte(s), &result)
	return result
}

func InterfaceObjectIdToString(d interface{}) string {
	_id := strings.ReplaceAll(fmt.Sprintf("%v", d), "ObjectID(\"", "")
	_id = strings.ReplaceAll(_id, "\")", "")
	return _id
}
