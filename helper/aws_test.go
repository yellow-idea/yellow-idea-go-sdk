package helper

import (
	"fmt"
	"testing"
)

func TestS3Base64ToCDN(t *testing.T) {
	//s := S3Base64ToCDN("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAABg2lDQ1BzUkdCIElFQzYxOTY2LTIuMQAAKJF1kbtLA0EQh78kio9EFLSwUAkSrYzECEEbwYioECTECL6a5MxDyOO4S5BgK9gGFEQbX4X+BdoK1oKgKILY2Fgr2mg454wQETPL7Hz7251hdxas4ZSS1ms8kM7ktNCE3zk3v+Cse8JOJw104YkoujoaDAaoau+3WMx47TZrVT/3r9mXY7oClnrhEUXVcsKTwoHVnGrylnCbkowsC58I92lyQeEbU4+W+dnkRJk/TdbCoTGwtgg7E784+ouVpJYWlpfjSqfyys99zJc4YpnZGYnd4h3ohJjAj5MpxhnDxwDDMvtw46VfVlTJ93znT5OVXEVmlQIaKyRIkqNP1LxUj0mMix6TkaJg9v9vX/X4oLdc3eGH2kfDeO2Buk0oFQ3j48AwSodge4DzTCU/uw9Db6IXK5prD5rX4fSiokW34WwD2u/ViBb5lmzi1ngcXo6haR5ar6Bxsdyzn32O7iC8Jl91CTu70Cvnm5e+AIiqZ/WR2z2KAAAACXBIWXMAAC4jAAAuIwF4pT92AAAADUlEQVQImWPYs2fPCgAHSQLddLPBfQAAAABJRU5ErkJggg==")
	//fmt.Println(len(s))
	s := S3Base64ToCDN("https://")
	fmt.Println(s)
}

func TestBinary(t *testing.T) {
	b := []byte("")
	fmt.Println(b)

	s := string([]byte{114, 56, 122, 108, 71, 57, 76, 83, 116, 87, 114, 105, 82, 105, 48, 113, 69, 88, 90, 53, 104, 89, 69, 69, 69, 70, 52, 106, 80, 107, 83, 103, 98, 86, 54, 105, 107, 84, 66, 122})
	fmt.Println(s)
}
