package helper

import "time"

func GetTimeNowGMT() time.Time {
	return time.Now().UTC().Add(7 * time.Hour)
}

func GetTimeFormat1(t time.Time) string {
	return t.Format("2006-01-02 15:04")
}
