package mongodb

import (
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"testing"
)

func TestMongoClient(t *testing.T) {
	client, db, err1 := MongoClient("UNILEVER_BHP")
	if err1 != nil {
		fmt.Print(err1)
	}
	defer client.Disconnect(context.TODO())

	collection := db.Collection("bcrm_line_point2")

	type Trainer struct {
		Name     string `json:"name" bson:"name"`
		Age      int    `json:"age" bson:"age"`
		City     string `json:"city" bson:"city"`
		LastName string `json:"last_name" bson:"last_name"`
	}
	// Insert One
	ash := &Trainer{"Ash", 10, "Pallet Town", "Jittanupagorn"}
	insertResult, err := collection.InsertOne(context.TODO(), ash)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Inserted a single document: ", insertResult.InsertedID)

	// Insert Many
	misty := &Trainer{"Misty", 10, "Cerulean City", "xO"}
	brock := &Trainer{"Brock", 15, "Pewter City", "Xs"}
	trainers := []interface{}{misty, brock}

	insertManyResult, err := collection.InsertMany(context.TODO(), trainers)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Inserted multiple documents: ", insertManyResult.InsertedIDs)

	// Update a document
	filter := &bson.D{{"name", "Ash"}}
	update := &bson.D{
		{"$inc", &bson.D{{"age", 999}}},
	}
	updateResult, err := collection.UpdateMany(context.TODO(), filter, update)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Matched %v documents and updated %v documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)

	// Find a single document
	var result *Trainer

	err = collection.FindOne(context.TODO(), filter).Decode(&result)
	if err != nil {
		log.Fatal(err)
	}

	xData, _ := json.Marshal(result)
	fmt.Printf("Find One : %+v\n", string(xData))

	// Find Multiple
	findOptions := options.Find()
	findOptions.SetLimit(2)

	var results []*Trainer

	cur, err := collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		var elem Trainer
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		results = append(results, &elem)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	_ = cur.Close(context.TODO())
	xDataList, _ := json.Marshal(results)
	fmt.Printf("Find All : %+v\n", string(xDataList))

	//Delete
	//idPrimitive, _ := primitive.ObjectIDFromHex("5eb1f3a0fb46ef61ec4b3061")
	deleteResult, err := collection.DeleteMany(context.TODO(), bson.D{})

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(deleteResult)
}
