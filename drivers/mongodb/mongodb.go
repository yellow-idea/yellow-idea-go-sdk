package mongodb

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	typesSDK "gitlab.yellow-idea.com/yellow-idea-go-sdk/types"
	"time"
)

type MongoClientType func(*typesSDK.SecretMongoDetail, string) (*mongo.Client, *mongo.Database, error)

func MongoClient(detail *typesSDK.SecretMongoDetail, dbName string) (*mongo.Client, *mongo.Database, error) {
	uri := fmt.Sprintf("mongodb://%s:%s@%s:27017/?authSource=admin", detail.Username, detail.Password, detail.IPAddress)
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		return nil, nil, err
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		return nil, nil, err
	}

	fmt.Println("Connected to MongoDB!")

	return client, client.Database(dbName), nil
}
