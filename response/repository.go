package response

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func ResponseRepoConnectionFail(err error) *gin.H {
	fmt.Println(err)
	return &gin.H{
		"code_return": -2,
		"message":     "Connection Fail",
	}
}

func ResponseRepoBadRequest(err error) *gin.H {
	fmt.Println(err)
	return &gin.H{
		"code_return": 2,
		"message":     "Bad Request",
	}
}
