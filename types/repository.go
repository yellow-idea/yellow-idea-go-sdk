package types

type SaveRepositoryResponse struct {
	ID         interface{} `json:"id,omitempty" bson:"id"`
	CodeReturn int8        `json:"code_return,omitempty" bson:"code_return"`
	Message    string      `json:"message,omitempty" bson:"message"`
	Data       interface{} `json:"data,omitempty" bson:"data"`
	Rows       interface{} `json:"rows,omitempty" bson:"rows"`
	Total      int32       `json:"total,omitempty" bson:"total"`
}
