package types

type SecretMongoDetail struct {
	IPAddress string
	Username  string
	Password  string
}
type SecretMySqlDetail struct {
	IPAddress string
	Username  string
	Password  string
}