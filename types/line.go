package types

type LineChanelDetail struct {
	ChannelID          string
	ChannelSecret      string
	ChannelAccessToken string
	RichMenuID         string
}
